FROM alpine

RUN apk add --no-cache openssh && ssh-keygen -A && \
    passwd -u root && \
    sed --in-place -E "s/^#?AllowTcpForwarding +no/AllowTcpForwarding yes/" /etc/ssh/sshd_config; \
    sed --in-place -E "s/^#?GatewayPorts +no/GatewayPorts yes/" /etc/ssh/sshd_config; \
    sed --in-place -E "s/^#?X11Forwarding +no/X11Forwarding yes/" /etc/ssh/sshd_config; \
    sed --in-place -E "s/^#?PermitTunnel +no/PermitTunnel yes/" /etc/ssh/sshd_config

EXPOSE 22

CMD ["/usr/sbin/sshd", "-e", "-D"]
